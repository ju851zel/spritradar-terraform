# Create a Pub/Sub topic on which the cloud function listens
resource "google_pubsub_topic" "spritradar_fetcher_pub_sub_topic" {
  name = local.gcp_cloud_function_fetcher_pub_sub_topic_name
}


