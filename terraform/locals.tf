locals {
  # bucket
  gcp_default_bucket                                = "spritradar-default-bucket-10"
  gcp_default_bucket_enterprise_customers_file_name = "enterprise_customers.json"
  gcp_default_bucket_ci_file_name                   = "gitlab-ci.json"
  gcp_prices_bucket                                 = "spritradar-prices-bucket-10"
  gcp_bucket_location                               = "EU"
  # cloud functions
  gcp_cloud_function_name                           = "spritradar-fetcher"
  cloud_function_main_class_name                    = "de.formigas.spritradar.fetcher.Application"
  # pubsub
  gcp_cloud_function_fetcher_pub_sub_topic_name     = "spritradar-fetcher-topic"
  # jobs
  gcp_cloud_scheduler_job_fetch_stations_name       = "spritradar-fetch-stations"
  gcp_cloud_scheduler_job_fetch_prices_name         = "spritradar-fetch-prices"
  # iam service accounts
  gcp_fetcher_service_account_id                    = "fetcher-firestore-communicator"
  gcp_gitlab_service_account_id                     = "gitlab-service-account"
  gcp_flotten_service_account_id                    = "flotten-service-account"
  gcp_async_service_account_id                      = "async-service-account"
  gcp_terraform_gitlab_service_account_id           = "terraform-git-account"
  gcp_backend_service_account_id                    = "backend-service-account"
  # archive file
  spritradar_archive_file                           = "spritradar-fetcher-fat-jar.jar"
  # artifact repo name
  artifact_repository_id                            = "spritradar-artifacts"
  artifact_backend_server_name                      = "spritradar-backend-server"
  # Kubernetes
  kubernetes_namespace                              = "k8s-namespace"
  pod_replicas                                      = 2
  certificate_name                                  = "spritradar-certificate"
  certificate_name_premium                          = "spritradar-certificate-premium"
  host_name                                         = "xyz.zellner.cc"
  public_ip_address_name                            = "spritradar-ingress-public-ip"
  public_ip_address_name_premium                    = "spritradar-ingress-public-ip-premium"
  images                                            = {
    backend  = "${var.gcp_region}-docker.pkg.dev/${var.gcp_project}/${local.artifact_repository_id}/spritradar-server"
    flotten  = "${var.gcp_region}-docker.pkg.dev/${var.gcp_project}/${local.artifact_repository_id}/spritradar-flotten-server"
    async    = "${var.gcp_region}-docker.pkg.dev/${var.gcp_project}/${local.artifact_repository_id}/spritradar-async-server"
    frontend = "${var.gcp_region}-docker.pkg.dev/${var.gcp_project}/${local.artifact_repository_id}/spritradar-frontend"
  }
  ports                                             = {
    backend  = 8080
    flotten  = 8081
    async    = 8082
    frontend = 80
  }
  app_base                                          = {
    backend  = {
      name           = "base-backend"
      pod_replicas   = 2
      readiness_path = "/base/api/stations"
    }
    flotten  = {
      name           = "base-flotten"
      pod_replicas   = 2
      readiness_path = "/base/flotten/alive"
    }
    async    = {
      name           = "base-async"
      pod_replicas   = 2
      readiness_path = "/base/async/alive"
    }
  }

  app_premium = {
    backend  = {
      name           = "premium-backend"
      pod_replicas   = 2
      readiness_path = "/premium/api/stations"
    }
    flotten  = {
      name           = "premium-flotten"
      pod_replicas   = 2
      readiness_path = "/premium/flotten/alive"
    }
    async    = {
      name           = "premium-async"
      pod_replicas   = 2
      readiness_path = "/premium/async/alive"
    }
    frontend = {
      name           = "premium-frontend"
      pod_replicas   = 2
      readiness_path = "/"
    }
  }
}
