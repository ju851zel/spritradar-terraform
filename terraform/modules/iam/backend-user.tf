resource "google_service_account" "spritradar_backend_service_account" {
  account_id   = var.gcp_backend_service_account_id
  display_name = var.gcp_backend_service_account_id
  description = "Account for reading the prices and stations from the Firestore and Cloud Storage"
}

# Create key for service account and assign it to service account
resource "google_service_account_key" "spritradar_backend_service_account_key" {
  service_account_id = google_service_account.spritradar_backend_service_account.name
}

output "backend-user-key-file" {
  value = google_service_account_key.spritradar_backend_service_account_key.private_key
  sensitive = true
}