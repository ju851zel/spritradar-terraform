# Account for writing in Firestore and Cloud Storage
resource "google_service_account" "spritradar_gitlab_service_account" {
  account_id   = var.gcp_gitlab_service_account_id
  display_name = var.gcp_gitlab_service_account_id
  description = "Account for publishing archives from gitlab"
}

# Create key for service account and assign it to service account
resource "google_service_account_key" "spritradar_gitlab_service_account_key" {
  service_account_id = google_service_account.spritradar_gitlab_service_account.name
}

output "gitlab-user-key-file" {
  value = google_service_account_key.spritradar_gitlab_service_account_key.private_key
  sensitive = true
}