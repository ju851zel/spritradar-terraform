# Account for writing in Firestore and Cloud Storage
resource "google_service_account" "terraform_gitlab_service_account" {
  account_id   = var.gcp_terraform_gitlab_service_account_id
  display_name = var.gcp_terraform_gitlab_service_account_id
  description = "Account for Terraform to run in gitlab ci"
}

# Create key for service account and assign it to service account
resource "google_service_account_key" "terraform_gitlab_service_account_key" {
  service_account_id = google_service_account.terraform_gitlab_service_account.name
}

output "terraform-gitlab-user-key-file" {
  value = google_service_account_key.terraform_gitlab_service_account_key.private_key
  sensitive = true
}