# Account for writing in Firestore and Cloud Storage
resource "google_service_account" "spritradar_fetcher_service_account" {
  account_id   = var.gcp_fetcher_service_account_id
  display_name = var.gcp_fetcher_service_account_id
  description = "Account for writing in Firestore and Cloud Storage from Cloud function"
}

# Create key for service account and assign it to service account
resource "google_service_account_key" "spritradar_fetcher_service_account_key" {
  service_account_id = google_service_account.spritradar_fetcher_service_account.name
}

output "fetcher-user-key-file" {
  value = google_service_account_key.spritradar_fetcher_service_account_key.private_key
  sensitive = true
}