variable "gcp_project" {
  type = string
}

variable "gcp_backend_service_account_id" {
  type = string
}

variable "gcp_fetcher_service_account_id" {
  type = string
}

variable "gcp_terraform_gitlab_service_account_id" {
  type = string
}

variable "gcp_async_service_account_id" {
  type = string
}

variable "gcp_flotten_service_account_id" {
  type = string
}

variable "gcp_gitlab_service_account_id" {
  type = string
}