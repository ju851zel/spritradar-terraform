#async
resource "google_project_iam_binding" "object_viewer" {
  project = var.gcp_project
  role    = "roles/storage.objectViewer"
  members = [
    "serviceAccount:${google_service_account.spritradar_async_service_account.email}",
    "serviceAccount:${google_service_account.spritradar_backend_service_account.email}",
  ]
}
resource "google_project_iam_binding" "viewer" {
  project = var.gcp_project
  role    = "roles/viewer"
  members = [
    "serviceAccount:${google_service_account.spritradar_async_service_account.email}",
    "serviceAccount:${google_service_account.spritradar_backend_service_account.email}",
  ]
}
resource "google_project_iam_binding" "datastore_user" {
  project = var.gcp_project
  role    = "roles/datastore.user"
  members = [
    "serviceAccount:${google_service_account.spritradar_async_service_account.email}",
    "serviceAccount:${google_service_account.spritradar_fetcher_service_account.email}",
    "serviceAccount:${google_service_account.spritradar_flotten_service_account.email}",
    "serviceAccount:${google_service_account.spritradar_backend_service_account.email}"
  ]
}

resource "google_project_iam_binding" "storage_admin" {
  project = var.gcp_project
  role    = "roles/storage.admin"
  members = [
    "serviceAccount:${google_service_account.spritradar_flotten_service_account.email}",
  ]
}

# Add role for writing jar to bucket
resource "google_project_iam_binding" "storage_object_admin" {
  project = var.gcp_project
  role    = "roles/storage.objectAdmin"
  members = [
    "serviceAccount:${google_service_account.spritradar_gitlab_service_account.email}",
    "serviceAccount:${google_service_account.spritradar_fetcher_service_account.email}",
    "serviceAccount:${google_service_account.spritradar_flotten_service_account.email}",
    "serviceAccount:${google_service_account.terraform_gitlab_service_account.email}"
  ]
}

# Add role for writing to firestore to gitlab spritradar_gitlab_service_account
resource "google_project_iam_binding" "artifacts_writer" {
  project = var.gcp_project
  role    = "roles/artifactregistry.writer"
  members = [
    "serviceAccount:${google_service_account.spritradar_gitlab_service_account.email}"
  ]
}