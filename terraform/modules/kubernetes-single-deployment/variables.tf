variable "kubernetes_namespace" {
  type        = string
  description = "Namespace for the deployment"
}

variable "pod_replicas" {
  type = number
}

variable "tenant" {
  type = string
}

variable "env_vars" {
  type = object({
    CLOUD_BUCKET = string
    CLOUD_CREDENTIALS = string
    SERVER_SERVLET_CONTEXT_PATH = string
  })
}

variable "deployment" {
  type = object({
    deployment_name = string
    service_name = string
    container  = object({
      name       = string
      image_name = string
      port       = number
      readiness_path = string
    })
    pod_label = string
  })
}
