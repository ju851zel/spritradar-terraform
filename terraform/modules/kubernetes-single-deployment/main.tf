terraform {
  required_version = "1.1.3"
}

resource "kubernetes_deployment" "deployment" {
  metadata {
    name      = var.deployment.deployment_name
    labels    = {
      deployment = var.deployment.deployment_name
    }
    namespace = var.kubernetes_namespace
  }
  spec {
    replicas = var.pod_replicas
    selector {
      match_labels = {
        app = var.deployment.pod_label
      }
    }
    template {
      metadata {
        labels = {
          app = var.deployment.pod_label
        }
      }
      spec {
        node_selector = {
          tenant = var.tenant
        }
        container {
          image = var.deployment.container.image_name
          name  = var.deployment.container.name
          image_pull_policy = "Always"
          port {
            container_port = var.deployment.container.port
          }
          readiness_probe {
            http_get {
              path = var.deployment.container.readiness_path
              port = var.deployment.container.port
            }
          }
          env {
            name  = "CLOUD_BUCKET"
            value = var.env_vars.CLOUD_BUCKET
          }
          env {
            name  = "CLOUD_CREDENTIALS"
            value = var.env_vars.CLOUD_CREDENTIALS
          }
          env {
            name  = "SERVER_SERVLET_CONTEXT_PATH"
            value = var.env_vars.SERVER_SERVLET_CONTEXT_PATH
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "deployment" {
  metadata {
    name      = var.deployment.service_name
    namespace = var.kubernetes_namespace
    labels    = {
      service = var.deployment.service_name
    }
  }
  spec {
    selector = {
      app = var.deployment.pod_label
    }
    type     = "NodePort"
    port {
      port        = 80
      target_port = var.deployment.container.port
    }
  }
}