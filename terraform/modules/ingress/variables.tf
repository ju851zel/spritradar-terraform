variable "kubernetes_namespace" {
  type = string
}

variable "public_ip" {
  type = string
}

variable "certificate_name" {
  type = string
}

variable "ingress_services" {
  type = list(object({
    name = string
    uri  = string
  }))
}