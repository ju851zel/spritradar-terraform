resource "kubernetes_ingress" "spritradar" {
  wait_for_load_balancer = true

  metadata {
    name        = "spritradar-ingress"
    namespace   = var.kubernetes_namespace
    annotations = {
      "kubernetes.io/ingress.global-static-ip-name" = var.public_ip
      "kubernetes.io/ingress.class"                 = "gce"
#      "kubernetes.io/ingress.class"                 = "nginx"
#      "nginx.ingress.kubernetes.io/rewrite-target"  = "/$1"
      "ingress.gcp.kubernetes.io/pre-shared-cert"   = var.certificate_name
    }
  }

  spec {
    rule {
#      host = "thesis.zellner.cc"
      http {
        dynamic "path" {
          for_each = var.ingress_services
          content {
            path      = "${path.value["uri"]}/*"
            backend {
              service_name = path.value["name"]
              service_port = 80
            }
          }
        }
      }
    }
  }
}

output "load_balancer_ip" {
  value = kubernetes_ingress.spritradar.status.0.load_balancer
}