module "kubernetes_deployment_server_backend" {
  source               = "../kubernetes-single-deployment"
  pod_replicas         = var.deployment.backend.pod_replicas
  deployment           = {
    deployment_name = "${var.deployment.backend.name}-deployment"
    service_name    = "service-${var.deployment.backend.name}"
    pod_label       = "${var.deployment.backend.name}-pod"
    container       = {
      name           = "${var.deployment.backend.name}-container"
      port           = var.deployment.backend.container_port
      readiness_path = var.deployment.backend.readiness_path
      image_name     = var.deployment.backend.image_name
    }
  }
  kubernetes_namespace = var.kubernetes_namespace
  env_vars             = {
    CLOUD_CREDENTIALS = base64decode(var.deployment.backend.user_key_file)
    CLOUD_BUCKET      = var.gcp_prices_bucket
    SERVER_SERVLET_CONTEXT_PATH = var.deployment.backend.uri_path
  }
  tenant               = var.tenant
}

module "kubernetes_deployment_flotten_server_backend" {
  source               = "../kubernetes-single-deployment"
  pod_replicas         = var.deployment.flotten.pod_replicas
  deployment           = {
    deployment_name = "${var.deployment.flotten.name}-deployment"
    service_name    = "service-${var.deployment.flotten.name}"
    pod_label       = "${var.deployment.flotten.name}-pod"
    container       = {
      name           = "${var.deployment.flotten.name}-container"
      port           = var.deployment.flotten.container_port
      readiness_path = var.deployment.flotten.readiness_path
      image_name     = var.deployment.flotten.image_name
    }
  }
  kubernetes_namespace = var.kubernetes_namespace
  env_vars             = {
    CLOUD_CREDENTIALS = base64decode(var.deployment.flotten.user_key_file)
    CLOUD_BUCKET      = var.gcp_default_bucket
    SERVER_SERVLET_CONTEXT_PATH = var.deployment.flotten.uri_path
  }
  tenant               = var.tenant
}

module "kubernetes_deployment_async_server_backend" {
  count = var.deploy_async_service ? 1 : 0
  source               = "../kubernetes-single-deployment"
  pod_replicas         = var.deployment.async.pod_replicas
  deployment           = {
    deployment_name = "${var.deployment.async.name}-deployment"
    service_name    = "service-${var.deployment.async.name}"
    pod_label       = "${var.deployment.async.name}-pod"
    container       = {
      name           = "${var.deployment.async.name}-container"
      port           = var.deployment.async.container_port
      readiness_path = var.deployment.async.readiness_path
      image_name     = var.deployment.async.image_name
    }
  }
  kubernetes_namespace = var.kubernetes_namespace
  env_vars             = {
    CLOUD_CREDENTIALS = base64decode(var.deployment.async.user_key_file)
    CLOUD_BUCKET      = var.gcp_prices_bucket
    SERVER_SERVLET_CONTEXT_PATH = var.deployment.async.uri_path
  }
  tenant               = var.tenant
}