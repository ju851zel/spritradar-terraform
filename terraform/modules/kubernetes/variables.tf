variable "kubernetes_namespace" {
  type        = string
  description = "Namespace for the deployment"
}

variable "gcp_prices_bucket" {
  type = string
}

variable "gcp_default_bucket" {
  type = string
}

variable "tenant" {
  type = string
}
variable "deploy_async_service" {
  type = bool
}

variable "deployment" {
  type = object({
    backend = object({
      name           = string
      container_port = number
      pod_replicas   = number
      readiness_path = string
      image_name     = string
      user_key_file  = string // module.iam-users.backend-user-key-file
      uri_path = string
    })
    flotten = object({
      name           = string
      container_port = number
      pod_replicas   = number
      readiness_path = string
      image_name     = string
      user_key_file  = string //module.iam-users.flotten-user-key-file
      uri_path = string
    })
    async   = object({
      name           = string
      container_port = number
      pod_replicas   = number
      readiness_path = string
      image_name     = string
      user_key_file  = string //module.iam-users.async-user-key-file
      uri_path = string
    })
  })
}
