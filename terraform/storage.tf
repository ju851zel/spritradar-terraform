# Create a bucket for storing the jar, containing the cloud function logic
resource "google_storage_bucket" "spritradar_default_bucket" {
  name          = local.gcp_default_bucket
  location      = local.gcp_bucket_location
#  force_destroy = true
}

# Create a bucket for storing the price data
resource "google_storage_bucket" "spritradar_prices_bucket" {
  name          = local.gcp_prices_bucket
  location      = local.gcp_bucket_location
#  force_destroy = true
}
