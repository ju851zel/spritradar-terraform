data "google_storage_bucket_object_content" "enterprise-customer-json" {
  name   = local.gcp_default_bucket_enterprise_customers_file_name
  bucket = local.gcp_default_bucket
}

output "enterprise-customer-data" {
  value = jsondecode(data.google_storage_bucket_object_content.enterprise-customer-json.content)
}


# Separately Managed Node Pool
resource "google_container_node_pool" "enterprise-nodes" {
  for_each = toset( jsondecode(data.google_storage_bucket_object_content.enterprise-customer-json.content) )
  name     = "pool-${lower(each.key)}"
  location = var.gcp_region

  cluster        = google_container_cluster.cluster.name
  node_count     = 1
  node_locations = [
    #    "${var.gcp_region}-b",
    "${var.gcp_region}-c",
    #    "${var.gcp_region}-d"
  ]

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
    ]

    labels = {
      env    = var.gcp_project
      tenant = each.key
    }

    machine_type = "e2-medium"
    tags         = ["gke-node", "${var.gcp_project}-gke", "test"]
    metadata     = {
      disable-legacy-endpoints = "true"
    }
  }

  depends_on = [google_container_cluster.cluster]
}

module "enterprise_deployment" {
  for_each             = toset(jsondecode(data.google_storage_bucket_object_content.enterprise-customer-json.content))
  source               = "./modules/kubernetes"
  deployment           = {
    backend = {
      name           = "${lower(each.key)}-backend"
      container_port = local.ports.backend
      pod_replicas   = local.app_base.backend.pod_replicas
      readiness_path = "/${each.key}/api/stations"
      image_name     = "${local.images.backend}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["backend"]}"
      user_key_file  = module.iam-users.backend-user-key-file
      uri_path       = "/${each.key}/api"
    }
    flotten = {
      name           = "${lower(each.key)}-flotten"
      container_port = local.ports.flotten
      pod_replicas   = local.app_base.flotten.pod_replicas
      readiness_path = "/${each.key}/flotten/alive"
      image_name     = "${local.images.flotten}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["flotten"]}"
      user_key_file  = module.iam-users.flotten-user-key-file
      uri_path       = "/${each.key}/flotten"
    }
    async   = {
      name           = "${lower(each.key)}-async"
      container_port = local.ports.async
      pod_replicas   = local.app_base.async.pod_replicas
      readiness_path = "/${each.key}/async/alive"
      image_name     = "${local.images.async}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["async"]}"
      user_key_file  = module.iam-users.async-user-key-file
      uri_path       = "/${each.key}/async"
    }
  }
  gcp_prices_bucket    = local.gcp_prices_bucket
  gcp_default_bucket   = local.gcp_default_bucket
  kubernetes_namespace = local.kubernetes_namespace
  depends_on           = [kubernetes_namespace.ns]
  tenant               = each.key
  deploy_async_service = true
}
