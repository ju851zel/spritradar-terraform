data "google_storage_bucket_object_content" "ci-json" {
  name   = local.gcp_default_bucket_ci_file_name
  bucket = local.gcp_default_bucket
}

output "docker-images-versions-data" {
  value = jsondecode(data.google_storage_bucket_object_content.ci-json.content)
}
