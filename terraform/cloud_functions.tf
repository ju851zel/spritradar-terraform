# Create a Cloud function listening on the Pub/Sub topic and running when a message is received
resource "google_cloudfunctions_function" "spritradar_fetch_mtsk_data" {
  name                  = local.gcp_cloud_function_name
  description           = "Triggering this fetches the newest data from the mtsk, making it accessible to the spritradar backend"
  runtime               = "java11"
  available_memory_mb   = 1024
  source_archive_bucket = google_storage_bucket.spritradar_default_bucket.name
  source_archive_object = "${local.spritradar_archive_file}.zip"
  timeout               = 120

  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = "projects/${var.gcp_project}/topics/${local.gcp_cloud_function_fetcher_pub_sub_topic_name}"
  }
  entry_point           = local.cloud_function_main_class_name
  depends_on            = [
    google_project_service.services,
  ]
  environment_variables = {
    CLOUD_BUCKET = local.gcp_prices_bucket
    CLOUD_CREDENTIALS = base64decode(module.iam-users.fetcher-user-key-file)
  }
}