# Use google cloud
provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
  credentials = file("./creds/serviceaccount.json")

}

provider "google-beta" {
  project = var.gcp_project
  region  = var.gcp_region
  credentials = file("./creds/serviceaccount.json")
}

data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${google_container_cluster.cluster.endpoint}"
  token                  =  data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(google_container_cluster.cluster.master_auth[0].cluster_ca_certificate)
}

