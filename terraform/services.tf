resource "google_project_service" "services" {
  for_each                   = toset([
    "cloudfunctions.googleapis.com",
    "cloudbuild.googleapis.com",
    "firestore.googleapis.com",
    "cloudscheduler.googleapis.com",
    "artifactregistry.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com"
  ])
  project                    = var.gcp_project
  service                    = each.key
  disable_dependent_services = true
  disable_on_destroy         = false
  timeouts {
    create = "3m"
    update = "3m"
  }
}
