terraform {
  required_version = "1.1.3"
  backend "gcs" {
    bucket  = "spritradar-test-10-terraform"
    prefix  = "terraform/state"
    credentials = "./creds/serviceaccount.json"
  }
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.7.1"
    }
    google = {
      source  = "hashicorp/google"
      version = "4.5.0"
    }
  }
}

# Create Cloud Projekt
# Enable App Engine
# Set Firestore to firestore mode
# create bucket terraform
# add owner role to terraform user
# enable identity plattform
# enable identitz platofr passowrd username login
// login for terraformgcloud auth application-default login
