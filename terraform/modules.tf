resource "kubernetes_namespace" "ns" {
  metadata {
    name = local.kubernetes_namespace
  }
  depends_on = [
    google_container_node_pool.base_nodes,
  ]
}

# GKE cluster
resource "google_container_cluster" "cluster" {
  name                     = "${var.gcp_project}-gke"
  location                 = var.gcp_region
  remove_default_node_pool = true
  initial_node_count       = 1
  network                  = google_compute_network.network.name
  subnetwork               = google_compute_subnetwork.subnet.name
  depends_on               = [google_project_service.services]
}

# Separately Managed Node Pool
resource "google_container_node_pool" "base_nodes" {
  name     = "${google_container_cluster.cluster.name}-base-pool"
  location = var.gcp_region

  cluster        = google_container_cluster.cluster.name
  node_count     = 1
  node_locations = [
    #    "${var.gcp_region}-b",
    #    "${var.gcp_region}-c",
    "${var.gcp_region}-d"
  ]

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
    ]

    labels = {
      env    = var.gcp_project
      tenant = "base"
    }

    machine_type = "e2-medium"
    tags         = ["gke-node", "${var.gcp_project}-gke", "test"]
    metadata     = {
      disable-legacy-endpoints = "true"
    }
  }

  depends_on = [google_container_cluster.cluster]
}

# Separately Managed Node Pool
resource "google_container_node_pool" "premium_nodes" {
  name     = "${google_container_cluster.cluster.name}-premium-pool"
  location = var.gcp_region

  cluster        = google_container_cluster.cluster.name
  node_count     = 1
  node_locations = [
    "${var.gcp_region}-b",
    #    "${var.gcp_region}-c",
    #    "${var.gcp_region}-d"
  ]

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
    ]

    labels = {
      env    = var.gcp_project
      tenant = "premium"
    }

    machine_type = "e2-standard-2"
    tags         = ["gke-node", "${var.gcp_project}-gke", "test"]
    metadata     = {
      disable-legacy-endpoints = "true"
    }
  }

  depends_on = [google_container_cluster.cluster]
}

data "google_project" "spritradar_project" {}

# Add role to default compute account which is used to pull from kubernees the images from artifact registry
resource "google_project_iam_binding" "spritradar_compute_service_account_k8s_permission_to_fetch_artifacts" {
  project    = var.gcp_project
  role       = "roles/artifactregistry.admin"
  members    = [
    "serviceAccount:${data.google_project.spritradar_project.number}-compute@developer.gserviceaccount.com"
  ]
  depends_on = [google_container_node_pool.base_nodes] // node pool must be created for user to exists
}


# --------------------- frontend --------------------
module "kubernetes_deployment_frontend" {
  source               = "./modules/kubernetes-single-deployment"
  pod_replicas         = local.app_premium.frontend.pod_replicas
  deployment           = {
    deployment_name = "${local.app_premium.frontend.name}-deployment"
    service_name    = "service-${local.app_premium.frontend.name}"
    pod_label       = "${local.app_premium.frontend.name}-pod"
    container       = {
      name           = "${local.app_premium.frontend.name}-container"
      port           = local.ports.frontend
      readiness_path = local.app_premium.frontend.readiness_path
      image_name     = "${local.images.frontend}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["frontend"]}"
    }
  }
  kubernetes_namespace = local.kubernetes_namespace
  depends_on           = [kubernetes_namespace.ns]
  env_vars             = {
    CLOUD_CREDENTIALS           = ""
    CLOUD_BUCKET                = ""
    SERVER_SERVLET_CONTEXT_PATH = ""
  }
  tenant               = "premium"
}

# -------------------- VPC -------------------------
resource "google_compute_global_address" "ip" {
  name = local.public_ip_address_name
}
# VPC
resource "google_compute_network" "network" {
  name                    = "${var.gcp_project}-vpc"
  auto_create_subnetworks = "false"
  depends_on              = [google_project_service.services]
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.gcp_project}-subnet"
  region        = var.gcp_region
  network       = google_compute_network.network.name
  ip_cidr_range = "10.10.0.0/24"
  depends_on    = [google_project_service.services]
}

resource "google_compute_firewall" "ssh-rule" {
  name          = "${google_container_cluster.cluster.name}-ssh-rule"
  network       = google_compute_network.network.name
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = ["0.0.0.0/0"]
  depends_on    = [google_project_service.services]
}

#-------------------- deployment --------------------
module "base_deployment" {
  source               = "./modules/kubernetes"
  deployment           = {
    backend = {
      name           = local.app_base.backend.name
      container_port = local.ports.backend
      pod_replicas   = local.app_base.backend.pod_replicas
      readiness_path = local.app_base.backend.readiness_path
      image_name     = "${local.images.backend}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["backend"]}"
      user_key_file  = module.iam-users.backend-user-key-file
      uri_path       = "/base/api"
    }
    flotten = {
      name           = local.app_base.flotten.name
      container_port = local.ports.flotten
      pod_replicas   = local.app_base.flotten.pod_replicas
      readiness_path = local.app_base.flotten.readiness_path
      image_name     = "${local.images.flotten}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["flotten"]}"
      user_key_file  = module.iam-users.flotten-user-key-file
      uri_path       = "/base/flotten"
    }
    async   = {
      name           = local.app_base.async.name
      container_port = local.ports.async
      pod_replicas   = local.app_base.async.pod_replicas
      readiness_path = local.app_base.async.readiness_path
      image_name     = "${local.images.async}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["async"]}"
      user_key_file  = module.iam-users.async-user-key-file
      uri_path       = "/base/async"
    }
  }
  gcp_prices_bucket    = local.gcp_prices_bucket
  gcp_default_bucket   = local.gcp_default_bucket
  kubernetes_namespace = local.kubernetes_namespace
  depends_on           = [kubernetes_namespace.ns]
  tenant               = "base"
  deploy_async_service = false
}

module "premium_deployment" {
  source               = "./modules/kubernetes"
  deployment           = {
    backend = {
      name           = local.app_premium.backend.name
      container_port = local.ports.backend
      pod_replicas   = local.app_premium.backend.pod_replicas
      readiness_path = local.app_premium.backend.readiness_path
      image_name     = "${local.images.backend}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["backend"]}"
      user_key_file  = module.iam-users.backend-user-key-file
      uri_path       = "/premium/api"
    }
    flotten = {
      name           = local.app_premium.flotten.name
      container_port = local.ports.flotten
      pod_replicas   = local.app_premium.flotten.pod_replicas
      readiness_path = local.app_premium.flotten.readiness_path
      image_name     = "${local.images.flotten}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["flotten"]}"
      user_key_file  = module.iam-users.flotten-user-key-file
      uri_path       = "/premium/flotten"
    }
    async   = {
      name           = local.app_premium.async.name
      container_port = local.ports.async
      pod_replicas   = local.app_premium.async.pod_replicas
      readiness_path = local.app_premium.async.readiness_path
      image_name     = "${local.images.async}:${jsondecode(data.google_storage_bucket_object_content.ci-json.content)["async"]}"
      user_key_file  = module.iam-users.async-user-key-file
      uri_path       = "/premium/async"
    }
  }
  gcp_prices_bucket    = local.gcp_prices_bucket
  gcp_default_bucket   = local.gcp_default_bucket
  kubernetes_namespace = local.kubernetes_namespace
  depends_on           = [kubernetes_namespace.ns]
  tenant               = "premium"
  deploy_async_service = true
}

module "gateway" {
  source               = "./modules/ingress"
  kubernetes_namespace = local.kubernetes_namespace
  depends_on           = [kubernetes_namespace.ns]
  #  ingress_config_host_name = var.host_name
  certificate_name     = local.certificate_name
  public_ip            = local.public_ip_address_name
  ingress_services     = flatten([
    flatten([
    for it in toset( jsondecode(data.google_storage_bucket_object_content.enterprise-customer-json.content) ) :
    [
      {
        name = "service-${lower(it)}-backend"
        uri  = "/${it}/api"
      },
      {
        name = "service-${lower(it)}-flotten"
        uri  = "/${it}/flotten"
      },
      {
        name = "service-${lower(it)}-async"
        uri  = "/${it}/async"
      }
    ]
    ]), {
      name = "service-${local.app_premium.frontend.name}"
      uri  = ""
    },
    {
      name = "service-${local.app_premium.backend.name}"
      uri  = "/premium/api"
    },
    {
      name = "service-${local.app_premium.flotten.name}"
      uri  = "/premium/flotten"
    },
    {
      name = "service-${local.app_premium.async.name}"
      uri  = "/premium/async"
    },
    {
      name = "service-${local.app_base.backend.name}"
      uri  = "/base/api"
    },
    {
      name = "service-${local.app_base.flotten.name}"
      uri  = "/base/flotten"
    },
  ])
}

output "load_balancer_ip" {
  value = module.gateway.load_balancer_ip
}

resource "google_compute_managed_ssl_certificate" "spritradar" {
  name = local.certificate_name
  managed {
    domains = [local.host_name]
  }
}
