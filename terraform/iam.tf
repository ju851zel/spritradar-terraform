module "iam-users" {
  source                                  = "./modules/iam"
  gcp_project                             = var.gcp_project
  gcp_fetcher_service_account_id          = local.gcp_fetcher_service_account_id
  gcp_gitlab_service_account_id           = local.gcp_gitlab_service_account_id
  gcp_backend_service_account_id          = local.gcp_backend_service_account_id
  gcp_flotten_service_account_id          = local.gcp_flotten_service_account_id
  gcp_async_service_account_id            = local.gcp_async_service_account_id
  gcp_terraform_gitlab_service_account_id = local.gcp_terraform_gitlab_service_account_id
}

output "iam-gitlab-user-key" {
  value     = module.iam-users.gitlab-user-key-file
  sensitive = true
}

output "iam-fetcher-user-key" {
  value     = module.iam-users.fetcher-user-key-file
  sensitive = true
}

output "iam-backend-user-key" {
  value     = module.iam-users.backend-user-key-file
  sensitive = true
}

output "iam-flotten-user-key" {
  value     = module.iam-users.flotten-user-key-file
  sensitive = true
}

output "iam-async-user-key" {
  value     = module.iam-users.async-user-key-file
  sensitive = true
}

output "iam-terraform-gitlab-user-key" {
  value     = module.iam-users.terraform-gitlab-user-key-file
  sensitive = true
}