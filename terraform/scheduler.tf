# Create a Scheduler job that sends a message 'stations' every 24hrs to the Pub/Sub topic
resource "google_cloud_scheduler_job" "spritradar_fetch_stations_job" {
  name        = local.gcp_cloud_scheduler_job_fetch_stations_name
  description = "This job triggers every 24h at 03:00 o' clock to fetch the current stations from mtsk"
  schedule    = "0 3 * * *"
  region      = var.gcp_region
  time_zone   = "Europe/Berlin"

  pubsub_target {
    topic_name = "projects/${var.gcp_project}/topics/${local.gcp_cloud_function_fetcher_pub_sub_topic_name}"
    data       = base64encode("stations")
  }
  depends_on = [
    google_project_service.services,
  ]
}

# Create a Scheduler job that sends a message 'prices' every 10 mins to the Pub/Sub topic
resource "google_cloud_scheduler_job" "spritradar_fetch_prices_job" {
  name        = local.gcp_cloud_scheduler_job_fetch_prices_name
  description = "This job triggers every 10 min a fetch of the current prices from mtsk"
  schedule    = "*/10 * * * *"
  region      = var.gcp_region
  time_zone   = "Europe/Berlin"

  pubsub_target {
    topic_name = "projects/${var.gcp_project}/topics/${local.gcp_cloud_function_fetcher_pub_sub_topic_name}"
    data       = base64encode("prices")
  }
  depends_on = [
    google_project_service.services,
  ]
}
