## --------------------- frontend --------------------
#module "kubernetes_deployment_frontend" {
#  source               = "./modules/kubernetes-single-deployment"
#  pod_replicas         = local.k82_deploy_base_user.frontend.pod_replicas
#  deployment           = {
#    deployment_name = "${local.k82_deploy_base_user.frontend.name}-deployment"
#    service_name    = "${local.k82_deploy_base_user.frontend.name}-service"
#    pod_label       = "${local.k82_deploy_base_user.frontend.name}-pod"
#    container       = {
#      name           = "${local.k82_deploy_base_user.frontend.name}-container"
#      port           = local.k82_deploy_base_user.frontend.container_port
#      readiness_path = local.k82_deploy_base_user.frontend.readiness_path
#      image_name     = local.k82_deploy_base_user.frontend.image_name
#    }
#  }
#  kubernetes_namespace = local.kubernetes_namespace_base
#  depends_on           = [kubernetes_namespace.base]
#  env_vars             = {
#    CLOUD_CREDENTIALS = ""
#    CLOUD_BUCKET      = ""
#  }
#}