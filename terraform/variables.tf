variable "gcp_project" {
  type = string
  default = "spritradar-test-10"
}

variable "gcp_region" {
  type = string
  default = "europe-west1"
}

variable "gcp_region_premium" {
  type = string
  default = "europe-west6"
}