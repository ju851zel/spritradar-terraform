resource "google_artifact_registry_repository" "spritradar-artifact-repo" {
  provider = google-beta

  location = var.gcp_region
  repository_id = local.artifact_repository_id
  description = "A repository for hosting spritradar docker images"
  format = "DOCKER"
  depends_on = [google_project_service.services]
}