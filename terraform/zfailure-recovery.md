# When terraform destroy namespace hangs run

```shell
gcloud container clusters get-credentials spritradar-test-6-gke --zone europe-west3-a --project spritradar-test-6
```

```shell
kubectl get namespace spritradar-namespace -o json > logging.json
```

remove all in finalizers

```shell
kubectl replace --raw "/api/v1/namespaces/spritradar-namespace/finalize" -f ./logging.json
```

