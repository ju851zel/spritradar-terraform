
# Create App Engine (necessary for functions), this will not be deleted with 'terraform destroy' once created,
#resource "google_app_engine_application" "app-engine" {
#  project     = local.gcp_project
#  location_id = local.gcp_region
#  database_type = "CLOUD_FIRESTORE"
#  depends_on = [
#    google_project_service.cloud_firestore_api
#  ]
#}


1. Create google project, add a billing account and enable app engine
2. Run terraform, this will create all basic resources.
   When trying to deploy the cloud function it will fail because no jar is available. This is intended.
3. Run ```terraform output iam-gitlab-user-key | base64 -d``` and paste the output into the gitlab ci google credentials variable.
4. Run the gitlab ci pipeline for deploying the jar of the cloud function and the docker image for the backend server.
5. Run terraform again
